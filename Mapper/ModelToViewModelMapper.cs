﻿using System;

namespace mtvmtm
{
	public class ModelToViewModelMapper : BaseMapper
	{
		static void MapModelToViewModel(string propertyName, Model model, ViewModel viewModel)
		{
			var modelPropertyValue = model.GetType ().GetProperty (propertyName).GetValue (model);
			viewModel.GetType().GetProperty (propertyName).SetValue (viewModel, modelPropertyValue);
		}

		public static ViewModel ModelToViewModel(Model model, ViewModel viewModel)
		{
			Map (model, viewModel, MapModelToViewModel);
			return viewModel;
		}
	}
}

