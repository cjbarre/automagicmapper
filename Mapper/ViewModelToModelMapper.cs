﻿using System;
using System.Reflection;
using System.Linq;

namespace mtvmtm
{
	public class ViewModelToModelMapper : BaseMapper
	{
		static void MapViewModelToModel(string propertyName, Model model, ViewModel viewModel)
		{
			var viewModelPropertyValue = viewModel.GetType ().GetProperty (propertyName).GetValue (viewModel);
			model.GetType().GetProperty (propertyName).SetValue (model, viewModelPropertyValue);
		}

		public static Model ViewModelToModel(Model model, ViewModel viewModel)
		{
			Map (model, viewModel, MapViewModelToModel);
			return model;
		}
	}
}

