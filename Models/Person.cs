﻿using System;

namespace mtvmtm
{
	public class Person : Model
	{
		public string FirstName 	{ get; set; }
		public string LastName 		{ get; set; }
		public string SecretInfo 	{ get; set; }

		public Person(string FirstName, string LastName, string SecretInfo) : base()
		{
			this.FirstName = FirstName;
			this.LastName = LastName;
			this.SecretInfo = SecretInfo;
		}

		public Person() : base()
		{

		}
	}
}

