﻿using System;

namespace mtvmtm
{
	public class PersonVM : ViewModel
	{
		public string FirstName 	{ get; set; }
		public string LastName 		{ get; set; }

		public PersonVM(string FirstName, string LastName) : base()
		{
			this.FirstName = FirstName;
			this.LastName = LastName;
		}

		public PersonVM() : base()
		{
		}
	}
}

