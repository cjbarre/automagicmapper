﻿using System;
using System.Linq;
using System.Reflection;

namespace mtvmtm
{
	public abstract class BaseMapper
	{
		protected delegate void MapDirector (string propertyName, Model model, ViewModel viewModel);

		protected static void Map(Model newModel, ViewModel viewModel, MapDirector d)
		{
			var modelType = newModel.GetType ();
			var viewModelType = viewModel.GetType ();

			var modelProperties = modelType.GetProperties ().Select (x => x.Name);

			var viewModelProperties = viewModelType.GetProperties ().Select (x => x.Name);
				
			var propertiesToMap = viewModelProperties.Intersect (modelProperties);

			foreach (var propertyName in propertiesToMap)
			{
				d (propertyName, newModel, viewModel);
			}
		}
	}
}

